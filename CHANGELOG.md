# Changelog

## Minify JS 3.0.2, 2024-09-06

Changes since 3.0.1:

- Issue #3465974 - Add gitlab support.
- Issue #3433458 - Support D11

## Minify JS 3.0.1, 2024-01-15

Changes since 3.0:

- Issue #3413312 - Drupal 10.1 JSOptimizer

## Minify JS 3.0, 2023-04-18

Changes since 8.x-2.2:

- Issue #3302727: Minify already minified script?
- Issue #3264637: What does the module do exactly? (Module description page)
- Issue #3321815: Replace README.txt with README.md
- Issue #3244979: PHP Fatal Error Argument 4 passed to Drupal\minifyjs\MinifyJs (line 58)
- Issue #3297674: Automated Drupal 10 compatibility fixes
- Issue #3211954: JSqueeze library in Minify JS is failing for await and async operator in JS
- Issue #3245042: JSqueeze is abandoned and does not support template literals
